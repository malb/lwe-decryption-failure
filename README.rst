LWE Decryption Failure
======================

You’ll need to compile ``arith.pyx`` first:

.. code-block:: bash

    $ sage -sh
    $ python setup.py build_ext --inplace

Estimate noise for various LWE-like encryption schemes:

.. code-block:: python
  
    sage: attach("arith.pyx")
    sage: attach("error.py")
    sage: kyber = Kyber()
    sage: kyber()
    -142.65720...

    sage: uround2 = URound2(as_submission=True)
    sage: uround2()
    -64.657022...

Compute size of `q` required to achieve target (estimated) decryption failure rate:

.. code-block:: python
  
    sage: attach("arith.pyx")
    sage: attach("error.py")
    sage: lima = Lima(n=1024, q=133121,sp=False)
    sage: lima.min_bound(128+16)*4
    26688

This respository is essentially `Kyber’s noise growth script <https://github.com/pq-crystals/kyber/blob/master/scripts/proba_util.py>`__ with some window dressing. In particular, everything that's nontrivial in ``RandomVariable`` was written by Léo Ducas, Vadim Lyubashevsky and John M. Schanck. Martin R. Albrecht is likely responsible for all bugs and made the following changes:

- random variable focused API
- object-oriented API
- additional distributions such as (sparse) uniform, discrete Gaussian etc.
- faster addition/subtraction using C++ maps in arith.pyx
- caching to speed-up computations
- utility functions/classes for adding new schemes, finding parameters, plotting distribution PDFs
- precision is not hardcoded (though that seems to make little difference)
