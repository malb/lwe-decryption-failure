from distutils.core import setup
from Cython.Build import cythonize

setup(
    name='arithmetic',
    ext_modules=cythonize("arith.pyx", language="c++"),
)
