# -*- coding: utf-8 -*-

"""
Estimate decryption failures for LWE-like schemes.

..  moduleauthor:: Léo Ducas (I think)

..  moduleauthor:: Vadim Lyubashevsky

..  moduleauthor:: John M. Schanck

..  moduleauthor:: Martin R. Albrecht

Everything that's nontrivial in ``RandomVariable`` is from https://github.com/pq-crystals/kyber
written by Léo, Vadim and John. Martin then made the following changes:

    - random variable focused API
    - object-oriented API
    - additional distributions such as (sparse) uniform, discrete Gaussian etc.
    - faster addition/subtraction using C++ maps in arith.pyx
    - caching to speed-up computations
    - utility functions/classes for adding new schemes, finding parameters, plotting distribution
      PDFs
    - precision is not hardcoded (though that seems to make little difference)

EXAMPLE::

    sage: attach("arith.pyx")
    sage: attach("error.py")
    sage: kyber = Kyber()
    sage: kyber()
    -142.65720...

    sage: uround2 = URound2(as_submission=True)
    sage: uround2()
    -64.657022...

"""

from sage.all import binomial, ceil, cached_method, sqrt, ZZ, log, pi, exp, line
from warnings import warn

try:
    from arith import Arith
except ModuleNotFoundError:
    warn("Cannot import arith module.")


class RandomVariable(object):
    """
    Random Variables.
    """

    """
    We cut off any entries with probability  `< 2^{-320}` by default.

    ..  note:: All probabilities are stored as machine doubles.  Regardless of precision parameter.
    """

    DEFAULT_PREC=320

    def __init__(self, prec=None):
        """
        Initialize a new random variable.

        :param prec: When ``clean`` is called all entries with probability `< 2^{-prec}` are
            discarded.
        """

        if prec is None:
            prec = self.DEFAULT_PREC

        self._D = {}
        self._prec = prec

    @classmethod
    def CenteredUniform(cls, B, prec=None, zeros=True):
        """
        Create a random variable following the uniform distribution on `[-B, B]`.

        :param zeros: if ``True`` consider `[-B, B]\\{0}`
        """
        D = cls(prec=prec)

        if zeros:
            c = (2*B+1)
        else:
            c = (2*B)

        for i in range(-B, B+1):
            D[i] = 1./c
        if not zeros:
            D[0] = 0.0
        return D

    @classmethod
    def SparseCenteredUniform(cls, B, h, n, prec=None):
        """
        Create a random variable with h/n non-zero elements uniformly sampled from `[-B,…,-1,1,…, B]`
        """
        D = cls(prec=prec)
        D[0] = 1- (1.*h)/n

        prob_nonzero = 1 - D[0]

        for i in range(-B, B+1):
            if not i:
                continue
            D[i] = 1./(2*B) * prob_nonzero

        return D

    @classmethod
    def Uniform(cls, S, prec=None):
        """
        Create a random variable following a uniform distribution over a set `S`.
        """

        D = cls(prec=prec)
        prob = 1./len(S)
        for e in S:
            D[e] = prob

        return D

    @classmethod
    def CenteredBinomial(cls, eta, prec=None):
        """
        Create a random variable following a Binomial distribution centered at zero.

        :param eta: width
        :param prec: precision

        """
        D = cls(prec=prec)
        for i in range(-eta, eta+1):
            D[i] = binomial(2*eta, i+eta) / 2.**(2*eta)
        return cls.clean(D)

    @classmethod
    def DiscreteGaussian(cls, s, prec=None):
        """
        Create a random variable following a discrete Gaussian with standard deviation `s/\\sqrt{2π}`.
        """
        D = cls(prec=prec)
        prec = D.prec

        D[0] = 1./s
        i = 0
        while True:
            D[i] = float(exp(-pi * 1.* i**2/s**2))/s
            D[-i] = D[i]
            if D[i] < 2.**-prec:
                break
            i += 1

        return D

    @staticmethod
    def _mod_switch(x, q, p):
        """
        Modulus switching (rounding to a different discretization of the Torus).

        :param x: value to round (integer)
        :param q: input modulus (integer)
        :param rq: output modulus (integer)
        """
        return int(round(1. * p * x / q)) % p

    @staticmethod
    def _balance(x, q):
        """
        Return `x \\bmod q` represent in `[-q/2,…,q/2]`.
        """
        a = x % q
        if a < q//2:
            return a
        return a - q

    @classmethod
    def RoundingError(cls, q, p, prec=None):
        """
        Create a random variable corresponding to the rounding error going from `q → p`
        """
        D = cls(prec=prec)
        for x in range(q):
            y = RandomVariable._mod_switch(x, q, p)
            z = RandomVariable._mod_switch(y, p, q)
            d = RandomVariable._balance(x - z, q)
            D[d] += 1./q
        return cls.clean(D)

    def __getitem__(self, i):
        return self._D.get(i, 0)

    def __setitem__(self, i, x):
        self._D[i] = x

    def __iter__(self):
        return iter(self._D)

    def items(self):
        return self._D.items()

    def __repr__(self):
        return "c: %1.f, sigma: %.1f, prec: %3d"%(self.mean, sqrt(self.variance), self.prec)

    @property
    def mean(self):
        return sum(k*v for k, v in self.items())

    @property
    def variance(self):
        EX2 = sum(k**2 * v for k, v in self.items())
        return EX2 - self.mean

    @property
    def prec(self):
        return self._prec

    @property
    def max(self):
        return max(self.support)

    @property
    def min(self):
        return min(self.support)

    @property
    def loss(self):
        return 1-sum(self._D.values())

    @property
    def support(self):
        return sorted(k for k in self if self[k])

    def __add__(self, other):
        """
        Convolution of two laws (sum of independent variables from two input laws).
        """
        C = RandomVariable(prec=max(self.prec, other.prec))
        # for i, vi in self._D.items():
        #     for j, vj in other._D.items():
        #         C._D[i+j] = C._D.get(i+j, 0.0) + vi * vj
        Arith.add(C._D, self._D, other._D)  # noqa
        return C

    def __sub__(self, other):
        C = RandomVariable(prec=max(self.prec, other.prec))
        # for i, vi in self._D.items():
        #     for j, vj in other._D.items():
        #         C._D[i-j] = C._D.get(i-j, 0.0) + vi * vj
        Arith.sub(C._D, self._D, other._D)  # noqa
        return C

    def __mul__(self, other):
        C = RandomVariable(prec=max(self.prec, other.prec))
        for i, vi in self._D.items():
            for j, vj in other._D.items():
                C._D[i*j] = C._D.get(i*j, 0.0) + vi * vj
        return C

    @cached_method
    def sum(A, i):
        """
        Compute the ith-fold convolution of a distribution (using double-and-add)

        :param A: first input law (dictionnary)
        :param i: (integer)

        """
        D = RandomVariable(prec=A.prec)
        D[0] = 1.0
        for bit in ZZ(i).bits()[::-1]:
            D = RandomVariable.clean(D+D)
            if bit:
                D = A + D
        return D

    def clean(self, prec=None):
        """
        Remove entries with probabilities `< 2^{-prec}`
        """
        if prec is None:
            prec = self.prec

        if prec is None:
            return self

        B = RandomVariable(prec=prec)
        for k, v in self.items():
            if v > 2**-prec:
                B[k] = v
        return B

    def tail_probability(self, t):
        """
        Probability that a drawn from `D` is strictly greater than `t` in absolute value

        :param t: tail parameter (integer)
        """
        s = 0
        if t >= self.max:
            return 0
        # Summing in reverse for better numerical precision (assuming tails are decreasing)
        for i in range(int(ceil(t)), self.max)[::-1]:
            s += self[i] + self[-i]
        return s

    def plot(self, **kwds):
        return line([(k, self._D[k]) for k in sorted(self._D)], **kwds)

# Schemes


class Scheme(object):
    """
    Base class for encryption/encapsulation schemes.
    """
    def __init__(self, bound=None, amplify=None, prec=None, **kwds):
        if prec is None:
            prec = RandomVariable.DEFAULT_PREC

        self.prec = prec
        self.params = kwds

        if bound is None:
            self.bound = self.params["q"]//4
        else:
            self.bound = bound

        if amplify is None:
            self.amplify = self.params["n"]
        else:
            self.amplify = amplify

    def __getattr__(self, name):
        if name == "D":
            self.D = self._D()
            return self.D
        else:
            raise AttributeError("'%s' has no attribute '%s'"%(self.__class__, name))

    def _D(self):
        raise NotImplementedError

    def __call__(self, bound=None, amplify=None, prec=None):
        if bound is None:
            bound = self.bound
        if amplify is None:
            amplify = self.amplify
        if prec is None:
            prec = self.prec

        probability = self.D.tail_probability(bound)
        # addition of 1/2^prec avoids log of zero
        probability = amplify * probability + 2.**-prec
        return log(probability, 2).n()

    def min_bound(self, secpar):
        """
        Find a minimal-ish bound such that the probability of hitting that bound is < 2^-secpar.
        """

        if secpar < 0:
            raise ValueError("Security parameter must be a positive integer.")

        bound = ceil(sqrt(self.D.variance))

        # increase bound while decryption failure is too big
        while self(bound) > -secpar:
            bound = 2*bound

        # decrease bound again (additively) to make it tighter
        while True:
            if self(bound - 16) > -secpar:
                break
            bound -= 16  # TODO can't be bothered to deal with sub 16 steps
        return bound


class Kyber(Scheme):
    def __init__(self, n=256, k=3, q=7681, eta=4, d_u=11, d_v=3, d_t=11, prec=None):
        params = {"n": n, "k": k, "q": q, "eta": eta, "d_u": d_u, "d_v": d_v, "d_t": d_t}
        Scheme.__init__(self, prec=prec, **params)

    @cached_method
    def _D(self):
        params = self.params
        prec = self.prec

        s = RandomVariable.CenteredBinomial(params["eta"], prec=prec)
        e = RandomVariable.CenteredBinomial(params["eta"], prec=prec)
        r = RandomVariable.CenteredBinomial(params["eta"], prec=prec)
        e_1 = RandomVariable.CenteredBinomial(params["eta"], prec=prec)
        e_2 = RandomVariable.CenteredBinomial(params["eta"], prec=prec)

        c_t = RandomVariable.RoundingError(params["q"], 2**params["d_t"], prec=prec)
        c_v = RandomVariable.RoundingError(params["q"], 2**params["d_v"], prec=prec)
        c_u = RandomVariable.RoundingError(params["q"], 2**params["d_u"], prec=prec)

        n = params["k"] * params["n"]

        D = c_v + e_2 + RandomVariable.sum((e + c_t) * r, n) + RandomVariable.sum(s * (e_1 + c_u), n)

        return D


class Lima(Scheme):
    """

    EXAMPLES::

        sage: lima = Lima(n=1024,   q=133121, sp=False);  print lima.min_bound(128+16)*4
        sage: lima = Lima(n=2048,   q=184321, sp=False);  print lima.min_bound(128+16)*4
        sage: lima = Lima(n=1018, q=12521473, sp=True);   print lima.min_bound(128+16)*4
        sage: lima = Lima(n=1306, q=48181249, sp=True);   print lima.min_bound(128+16)*4
        sage: lima = Lima(n=1822, q=44802049, sp=True);   print lima.min_bound(128+16)*4
        sage: lima = Lima(n=2062, q=16900097, sp=True);   print lima.min_bound(128+16)*4

    """

    def __init__(self, n=1024, q=133121, eta=20, prec=None, sp=False):
        params = {"n": n, "q": q, "eta": eta}
        Scheme.__init__(self, prec=prec, **params)
        if sp:
            self._ell = 2*n
        else:
            self._ell = n

    @cached_method
    def _D(self):
        params = self.params
        prec = self.prec
        e_ = v = d = s = e = RandomVariable.CenteredBinomial(params["eta"], prec=prec)
        ell = self._ell

        D = RandomVariable.sum(e_ * v, ell) + d - RandomVariable.sum(s * e, ell)
        return D


class REmblem(Scheme):
    def __init__(self, n=512, q=2**14, sigma=3, B=1, prec=None):
        params = {"n": n, "q": q, "sigma": sigma, "B": B}
        Scheme.__init__(self, prec=prec, **params)

    @cached_method
    def _D(self):
        params = self.params
        prec = self.prec
        x = r = RandomVariable.CenteredUniform(B=params["B"], prec=prec)
        e = e1 = e2 = RandomVariable.DiscreteGaussian(params["sigma"]*sqrt(2*pi))

        D = RandomVariable.sum(r * e, params["n"]) + e2 - RandomVariable.sum(e1 * x, params["n"])
        return D


class URound2(Scheme):
    """

    .. note :: We restrict to the ring variant.

    """
    def __init__(self, n=522, h=78, q=2**15, p=2**8, t=2**3, B=1, key_size=256, as_submission=False, prec=None):
        params = {"n": n, "q": q, "p": p, "t": t, "B": B, "h": h, "as_submission": as_submission}
        Scheme.__init__(self, prec=prec, amplify=key_size, **params)

    @cached_method
    def _D(self):
        params = self.params
        as_submission = params["as_submission"]

        R = S = RandomVariable.CenteredUniform(B=params["B"], zeros=False)
        I_B = I_U = RandomVariable.RoundingError(params["q"], params["p"])
        I_v = RandomVariable.RoundingError(params["q"], params["t"])

        # I_B*R != _B since I_B is unbalanced but I_B*R is balanced.
        if as_submission:
            P1 = I_B
            P2 = I_U
        else:
            P1 = R*I_B
            P2 = I_U*S

        D  = RandomVariable.sum(P1, 2*params["h"])
        D += RandomVariable.sum(P2, 2*params["h"])
        D += I_B + I_v
        return D
