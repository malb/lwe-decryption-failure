# -*- coding: utf-8 -*-
# distutils: language = c++
from libcpp.unordered_map cimport unordered_map as map
from cython.operator cimport dereference as deref, preincrement as inc

class Arith(object):

    @staticmethod
    def add(dict C, dict A, dict B):
        cdef int k
        cdef double v
        cdef map[int,double] a
        cdef map[int,double] b
        cdef map[int,double] c

        cdef int h = max(A.keys())+max(B.keys())
        cdef int l = min(A.keys())+min(B.keys())

        for i in range(l, h+1):
            c[i] = 0.0

        for k, v in A.iteritems():
            a[k] = v
        for k, v in B.iteritems():
            b[k] = v

        cdef map[int,double].iterator ai = a.begin()
        cdef map[int,double].iterator bi

        while ai != a.end():
            bi = b.begin()
            while bi != b.end():
                c[deref(ai).first + deref(bi).first] += deref(ai).second * deref(bi).second
                inc(bi)
            inc(ai)

        for k in range(l, h+1):
            C[k] = c[k]

    @staticmethod
    def sub(dict C, dict A, dict B):
        cdef int k
        cdef double v
        cdef map[int,double] a
        cdef map[int,double] b
        cdef map[int,double] c

        cdef int h = max(A.keys())-min(B.keys())
        cdef int l = min(A.keys())-max(B.keys())

        for i in range(l, h+1):
            c[i] = 0.0

        for k, v in A.iteritems():
            a[k] = v
        for k, v in B.iteritems():
            b[k] = v

        cdef map[int,double].iterator ai = a.begin()
        cdef map[int,double].iterator bi

        while ai != a.end():
            bi = b.begin()
            while bi != b.end():
                c[deref(ai).first - deref(bi).first] += deref(ai).second * deref(bi).second
                inc(bi)
            inc(ai)

        for k in range(l, h+1):
            C[k] = c[k]
